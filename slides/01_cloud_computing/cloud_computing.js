/*
  order of backgrounds:
  - cloud_1
  - cloud_2
  - fog_1
  - fog_2
  - edge_1
  - edge_2
  - edge_3
  - dew_1
  - dew_2
  - dew_3
  */
Reveal.addEventListener( 'flavours', function( event ) {
  console.log("enter slide flavours");
  document.getElementById("flavours").setAttribute("data-background", "./files/cloud_1.svg");
  Reveal.syncSlide();
});
Reveal.addEventListener( 'fragmentshown', function( event ) {
  if(event.fragment.id === "cloud_2"){
    console.log("show cloud_2");
    document.getElementById("flavours").setAttribute("data-background", "./files/cloud_2.svg");
    Reveal.syncSlide();
  };
  if(event.fragment.id === "fog_1"){
    console.log("show fog_1");
    document.getElementById("flavours").setAttribute("data-background", "./files/fog_1.svg");
    Reveal.syncSlide();
  };
  if(event.fragment.id === "fog_2"){
    console.log("show fog_2");
    document.getElementById("flavours").setAttribute("data-background", "./files/fog_2.svg");
    Reveal.syncSlide();
  };
  if(event.fragment.id === "edge_1"){
    console.log("show edge_1");
    document.getElementById("flavours").setAttribute("data-background", "./files/edge_1.svg");
    Reveal.syncSlide();
  };
  if(event.fragment.id === "edge_2"){
    console.log("show edge_2");
    document.getElementById("flavours").setAttribute("data-background", "./files/edge_2.svg");
    Reveal.syncSlide();
  };
  if(event.fragment.id === "edge_3"){
    console.log("show edge_3");
    document.getElementById("flavours").setAttribute("data-background", "./files/edge_3.svg");
    Reveal.syncSlide();
  };
  if(event.fragment.id === "dew_1"){
    console.log("show dew_1");
    document.getElementById("flavours").setAttribute("data-background", "./files/dew_1.svg");
    Reveal.syncSlide();
  };
  if(event.fragment.id === "dew_2"){
    console.log("show dew_2");
    document.getElementById("flavours").setAttribute("data-background", "./files/dew_2.svg");
    Reveal.syncSlide();
  };
  if(event.fragment.id === "dew_3"){
    console.log("show dew_3");
    document.getElementById("flavours").setAttribute("data-background", "./files/dew_3.svg");
    Reveal.syncSlide();
  };
  //if(event.fragment.id === "next"){
  //  console.log("next");
  //  Reveal.nextFragment();
  //};
});
Reveal.addEventListener( 'fragmenthidden', function( event ) {
  if(event.fragment.id === "cloud_2"){
    console.log("hide cloud_2");
    document.getElementById("flavours").setAttribute("data-background", "./files/cloud_1.svg");
    Reveal.syncSlide();
  };
  if(event.fragment.id === "fog_1"){
    console.log("hide fog_1");
    document.getElementById("flavours").setAttribute("data-background", "./files/cloud_2.svg");
    Reveal.syncSlide();
  };
  if(event.fragment.id === "fog_2"){
    console.log("hide fog_2");
    document.getElementById("flavours").setAttribute("data-background", "./files/fog_1.svg");
    Reveal.syncSlide();
  };
  if(event.fragment.id === "edge_1"){
    console.log("hide edge_1");
    document.getElementById("flavours").setAttribute("data-background", "./files/fog_2.svg");
    Reveal.syncSlide();
  };
  if(event.fragment.id === "edge_2"){
    console.log("hide edge_2");
    document.getElementById("flavours").setAttribute("data-background", "./files/edge_1.svg");
    Reveal.syncSlide();
  };
  if(event.fragment.id === "edge_3"){
    console.log("hide edge_3");
    document.getElementById("flavours").setAttribute("data-background", "./files/edge_2.svg");
    Reveal.syncSlide();
  };
  if(event.fragment.id === "dew_1"){
    console.log("hide dew_1");
    document.getElementById("flavours").setAttribute("data-background", "./files/edge_3.svg");
    Reveal.syncSlide();
  };
  if(event.fragment.id === "dew_2"){
    console.log("hide dew_2");
    document.getElementById("flavours").setAttribute("data-background", "./files/dew_1.svg");
    Reveal.syncSlide();
  };
  if(event.fragment.id === "dew_3"){
    console.log("hide dew_3");
    document.getElementById("flavours").setAttribute("data-background", "./files/dew_2.svg");
    Reveal.syncSlide();
  };
  //if(event.fragment.id === "previous"){
  //  console.log("next");
  //  Reveal.prevFragment();
  //};
});
